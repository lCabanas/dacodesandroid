### Prueba Android

- Para poder ejecutar la aplicación es necesario tener instalado Android Studio, el instalador se encuentra en el siguiente enlace https://developer.android.com/studio

- Una vez instalado Android studio solo es necesario buscar el proyecto en el repositorio clonado.

- Al momento de abrir el proyecto Android Studio solicitara instalar los SDK necesarios para el proyecto (en caso de no tenerlos).

- Después de que Android Studio instale lo necesario para compilar el proyecto, se deberá esperar a que el gradle finalice con la descarga de las librerías.

- Para ejecutar la app en un teléfono, deberá activarse la opciones de desarrollador y la depuración USB de dicho teléfono, de igual manera deberán estar instalados los driver del teléfono.
