package com.dacodes.android.views.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.dacodes.android.R;
import com.dacodes.android.model.Wallet;
import com.dacodes.android.presenter.BenevitsPresenter;
import com.dacodes.android.views.activities.LoginActivity;
import com.dacodes.android.views.adapters.WalletAdapter;
import com.dacodes.android.views.viewcontrollers.base.BenevitsViewController;
import com.ethanhua.skeleton.Skeleton;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BenevitsFragment extends Fragment implements BenevitsViewController {

    @BindView(R.id.swipeRefreshGame)
    SwipeRefreshLayout mSwipeRefreshLayout;

    @BindView(R.id.recyclerViewWallet)
    RecyclerView mRecyclerView;

    BenevitsPresenter mPresenter;
    WalletAdapter mWalletAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = null;
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }

        try {
            view = inflater.inflate(R.layout.fragment_home, container, false);
            ButterKnife.bind(this, view);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        if(mPresenter == null){
            mPresenter = new BenevitsPresenter(this, getContext());
            mWalletAdapter = new WalletAdapter(new ArrayList<>(),getContext());
            LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
            mRecyclerView.setLayoutManager(layoutManager);
            mRecyclerView.setHasFixedSize(true);
            mRecyclerView.setAdapter(mWalletAdapter);
        }


        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeRefreshLayout.setRefreshing(false);
                mPresenter.wallets();
            }
        });

        mPresenter.wallets();
    }

    @Override
    public void successWallets(List<Wallet> wallets) {
        mRecyclerView.setVisibility(View.VISIBLE);
        mWalletAdapter = new WalletAdapter(wallets, getContext());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mWalletAdapter);
    }

    @Override
    public void errorWS(String error) {
        mRecyclerView.setVisibility(View.GONE);
        Toast.makeText(getContext(), "No se encontraron wallets", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void loadingWallet() {
        Skeleton.bind(mRecyclerView)
                .adapter(mWalletAdapter)
                .load(R.layout.row_skeleton)
                .show();
    }

    @Override
    public void endLoadingWallet() {

    }

    @Override
    public void emptyWallet() {
        mRecyclerView.setVisibility(View.GONE);
        Toast.makeText(getContext(), "No se encontraron wallets", Toast.LENGTH_SHORT).show();
    }
}
