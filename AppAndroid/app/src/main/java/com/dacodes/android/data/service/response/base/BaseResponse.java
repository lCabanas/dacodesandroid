package com.dacodes.android.data.service.response.base;

public class BaseResponse {
    public String error;

    public boolean isSuccess(){
        return (this.error == null || this.error.isEmpty());
    }
}
