package com.dacodes.android.presenter;

import android.content.Context;

import com.dacodes.android.R;
import com.dacodes.android.data.service.ClientService;
import com.dacodes.android.data.service.request.LoginRequest;
import com.dacodes.android.data.service.response.LoginResponse;
import com.dacodes.android.views.utils.SharedPrefsUtils;
import com.dacodes.android.views.viewcontrollers.base.BenevitsMainViewController;
import com.dacodes.android.views.viewcontrollers.base.LoginViewController;
import com.google.gson.Gson;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dacodes.android.data.service.ClientService.TOKEN;

public class LogoutPresenter {
    private BenevitsMainViewController mViewController;
    private ClientService mClientService;
    private Context mContext;

    public LogoutPresenter(BenevitsMainViewController mViewController, Context mContext) {
        this.mViewController = mViewController;
        this.mContext = mContext;
        if (this.mClientService == null) {
            this.mClientService = new ClientService();
        }
    }

    public void logout() {
        mViewController.showProgressDialog("Espere un momento...");
        String token = SharedPrefsUtils.getStringPreference(mContext, TOKEN);
        mClientService
                .getAPI()
                .logout(token)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        ResponseBody mResponse = response.body();
                        SharedPrefsUtils.setStringPreference(mContext, TOKEN, response.headers().get(TOKEN));
                        if (mResponse != null) {
                            mViewController.hideProgressDialog();
                            mViewController.successLogout();
                        } else {
                            mViewController.hideProgressDialog();
                            mViewController.errorLogout();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        mViewController.hideProgressDialog();
                        mViewController.errorLogout();
                    }
                });
    }
}
