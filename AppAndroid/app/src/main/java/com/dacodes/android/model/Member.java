package com.dacodes.android.model;

public class Member {
    public int id;
    public int user_id;
    public String id_socio_infonavit;
    public String name;
    public String lastname;
    public String mobile;
    public String zipcode;
}
