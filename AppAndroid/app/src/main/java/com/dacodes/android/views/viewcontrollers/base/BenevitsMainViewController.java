package com.dacodes.android.views.viewcontrollers.base;

import com.dacodes.android.model.Wallet;

import java.util.List;

public interface BenevitsMainViewController {
    void showProgressDialog(String text);
    void hideProgressDialog();
    void successLogout();
    void errorLogout();
}
