package com.dacodes.android.views.adapters.holders;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dacodes.android.R;
import com.dacodes.android.model.Wallet;
import com.dacodes.android.views.adapters.BenevitAdapter;
import com.dacodes.android.views.adapters.WalletAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WalletHolder extends BaseViewHolder {
    @BindView(R.id.textViewTitle)
    TextView mTextViewTitle;

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;

    public WalletHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void config(Wallet mWallet, Context mContext) {
        mTextViewTitle.setText(mWallet.name);

        mRecyclerView.setVisibility(View.VISIBLE);
        BenevitAdapter mAdapter = new BenevitAdapter(mContext, mWallet.benevits);
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
    }
}
