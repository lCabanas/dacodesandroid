package com.dacodes.android.views.viewcontrollers.base;

import com.dacodes.android.model.Member;

public interface LoginViewController {
    void successLogin(Member member);
    void errorLogin(String error);
    void showProgressDialog(String text);
    void hideProgressDialog();
}
