package com.dacodes.android.model;

import java.util.List;

public class Wallet implements Comparable<Wallet>{
    public int id;
    public int display_index;
    public String display_text;
    public String icon;
    public String path;
    public int max_level;
    public String avatar;
    public String name;
    public int benevit_count;
    public String mobile_cover_url = null;
    public String desktop_cover_url = null;
    public int member_level;
    public String primary_color;

    public List<Benevit> benevits;

    @Override
    public int compareTo(Wallet f) {

        if (display_index > f.display_index) {
            return 1;
        }
        else if (display_index <  f.display_index) {
            return -1;
        }
        else {
            return 0;
        }

    }
}
