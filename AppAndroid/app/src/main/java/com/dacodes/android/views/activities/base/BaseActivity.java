package com.dacodes.android.views.activities.base;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class BaseActivity extends AppCompatActivity {

    public void showConfirmDialog(String title, String msj , DialogInterface.OnClickListener mOnClickListenerYes){
        AlertDialog.Builder dialogConfirm = new AlertDialog.Builder(BaseActivity.this);
        dialogConfirm.setTitle(title);
        dialogConfirm.setMessage(msj);
        dialogConfirm.setPositiveButton("Si", mOnClickListenerYes);
        dialogConfirm.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int id) {
                dialogInterface.cancel();
            }
        });
        dialogConfirm.show();
    }

    public void showInfoDialog(String title, String msj, String button){
        AlertDialog.Builder dialogConfirm = new AlertDialog.Builder(BaseActivity.this);
        dialogConfirm.setTitle(title);
        dialogConfirm.setMessage(msj);
        dialogConfirm.setPositiveButton(button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        dialogConfirm.show();
    }

    public void hideKeyboard(){
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private ProgressDialog mProgressDialog;
    public void showOnProgressDialog(String message){
        try {
            if (mProgressDialog != null) {
                mProgressDialog.dismiss();
            }
            mProgressDialog = new ProgressDialog(BaseActivity.this);
            mProgressDialog.setMessage(message);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setProgress(0);
            mProgressDialog.setMax(100);
            mProgressDialog.show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void dismissProgressDialog(){
        try {
            if (mProgressDialog != null) {
                mProgressDialog.dismiss();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
