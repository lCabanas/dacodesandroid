package com.dacodes.android.data.service.response;

import com.dacodes.android.model.Benevit;

import java.util.List;

public class BenevitsResponse {

    public List<Benevit> locked;
    public List<Benevit> unlocked;
}
