package com.dacodes.android.views.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dacodes.android.R;
import com.dacodes.android.model.Benevit;
import com.dacodes.android.model.Wallet;
import com.dacodes.android.views.adapters.holders.BenevitHolder;
import com.dacodes.android.views.adapters.holders.WalletHolder;

import java.util.List;

public class BenevitAdapter extends RecyclerView.Adapter<BenevitHolder> {

    List<Benevit> mList;
    Context mContext;

    public BenevitAdapter(Context mContext, List<Benevit> mList) {
        this.mList = mList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public BenevitHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View master = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_benevit, parent, false);
        BenevitHolder mHolder = new BenevitHolder(master);
        return mHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull BenevitHolder holder, int position) {
        Benevit mItem = getItemAtPosition(position);
        holder.config(mItem, mContext);
    }

    @Override
    public int getItemCount() {
        return (mList == null ? 0 : mList.size());
    }

    public Benevit getItemAtPosition(int position) {
        if(position >= 0 && position < mList.size()){
            return mList.get(position);
        }
        return null;
    }
}
