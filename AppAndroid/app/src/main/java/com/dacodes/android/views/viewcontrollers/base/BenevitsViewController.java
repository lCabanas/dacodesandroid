package com.dacodes.android.views.viewcontrollers.base;

import com.dacodes.android.model.Wallet;

import java.util.List;

public interface BenevitsViewController {
    void successWallets(List<Wallet> wallets);
    void errorWS(String error);
    void loadingWallet();
    void endLoadingWallet();
    void emptyWallet();
}
