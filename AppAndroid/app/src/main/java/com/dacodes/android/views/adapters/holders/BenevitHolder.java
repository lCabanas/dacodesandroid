package com.dacodes.android.views.adapters.holders;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.dacodes.android.R;
import com.dacodes.android.model.Benevit;
import com.dacodes.android.model.Territories;
import com.dacodes.android.model.Wallet;

import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BenevitHolder extends BaseViewHolder {
    @BindView(R.id.relativeLocked)
    LinearLayout mLinearLayoutLocked;

    @BindView(R.id.relativeUnLocked)
    LinearLayout mLinearLayoutUnLocked;

    //Locked
    @BindView(R.id.imageLogoLocked)
    ImageView mImageViewLLocked;

    //UnLocked
    @BindView(R.id.imageLogoUnLocked)
    ImageView mImageViewUnLocked;

    @BindView(R.id.textViewDescription)
    TextView mTextViewDescription;

    @BindView(R.id.textViewTerritories)
    TextView mTextViewTerritories;

    @BindView(R.id.textViewExpiration)
    TextView mTextViewExpiration;

    public BenevitHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void config(Benevit mBenevit, Context mContext) {
        if(mBenevit.locked){
            mLinearLayoutLocked.setVisibility(View.VISIBLE);
            mLinearLayoutUnLocked.setVisibility(View.GONE);

            Glide.with(mContext)
                .load(mBenevit.vector_full_path)
                .into(mImageViewLLocked);

        }else{
            mLinearLayoutLocked.setVisibility(View.GONE);
            mLinearLayoutUnLocked.setVisibility(View.VISIBLE);

            Glide.with(mContext)
                    .load(mBenevit.ally.mini_logo_full_path)
                    .into(mImageViewUnLocked);

            mTextViewDescription.setText(mBenevit.description);

            String territories = "";
            for (Territories item : mBenevit.territories){
                territories = territories.isEmpty() ? item.name : "\n" + item.name;
            }

            territories = territories.isEmpty() ? "N/A": territories;
            mTextViewTerritories.setText(territories);

            long diff = mBenevit.expiration_date.getTime() - new Date().getTime();
            long diffDays = diff / (24 * 60 * 60 * 1000);
            mTextViewExpiration.setText("Expira en: " + diffDays + (diffDays > 1 ? " días" : " día"));
        }
    }
}
