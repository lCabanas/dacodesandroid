package com.dacodes.android.presenter;

import android.content.Context;

import com.dacodes.android.data.service.ClientService;
import com.dacodes.android.data.service.response.BenevitsResponse;
import com.dacodes.android.model.Benevit;
import com.dacodes.android.model.Wallet;
import com.dacodes.android.views.utils.SharedPrefsUtils;
import com.dacodes.android.views.viewcontrollers.base.BenevitsViewController;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dacodes.android.data.service.ClientService.TOKEN;

public class BenevitsPresenter {
    private BenevitsViewController mViewController;
    private ClientService mClientService;
    private Context mContext;

    public BenevitsPresenter(BenevitsViewController mViewController, Context mContext) {
        this.mViewController = mViewController;
        this.mContext = mContext;
        if (this.mClientService == null) {
            this.mClientService = new ClientService();
        }
    }

    public void wallets() {
        mViewController.loadingWallet();
        mClientService
                .getAPI()
                .wallets()
                .enqueue(new Callback<List<Wallet>>() {
                    @Override
                    public void onResponse(Call<List<Wallet>> call, Response<List<Wallet>> response) {
                        List<Wallet> mResponse = response.body();
                        if (mResponse != null && !mResponse.isEmpty()) {
                            String token = SharedPrefsUtils.getStringPreference(mContext, TOKEN);
                            mClientService
                                    .getAPI()
                                    .benevits(token)
                                    .enqueue(new Callback<BenevitsResponse>() {
                                        @Override
                                        public void onResponse(Call<BenevitsResponse> call, Response<BenevitsResponse> response2) {
                                            BenevitsResponse mBenevitsResponse = response2.body();
                                            if (mBenevitsResponse != null) {

                                                Collections.sort(mResponse);
                                                for (int x= 0; x < mResponse.size();x++) {
                                                    mResponse.get(x).benevits = new ArrayList<>();

                                                    for(Benevit locket : mBenevitsResponse.locked){
                                                        if(locket.wallet.name.equals(mResponse.get(x).name)){
                                                            locket.locked = true;
                                                            mResponse.get(x).benevits.add(locket);

                                                        }
                                                    }

                                                    for(Benevit unlocket : mBenevitsResponse.unlocked){
                                                        if(unlocket.wallet.name.equals(mResponse.get(x).name)){
                                                            unlocket.locked = false;
                                                            mResponse.get(x).benevits.add(unlocket);

                                                        }
                                                    }
                                                }

                                                mViewController.endLoadingWallet();
                                                mViewController.successWallets(mResponse);
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<BenevitsResponse> call, Throwable t) {
                                            mViewController.endLoadingWallet();
                                            mViewController.emptyWallet();
                                        }
                                    });


                        } else {
                            mViewController.endLoadingWallet();
                            mViewController.emptyWallet();
                        }
                    }

                    @Override
                    public void onFailure(Call<List<Wallet>> call, Throwable t) {
                        mViewController.endLoadingWallet();
                        mViewController.emptyWallet();
                    }
                });
    }
}
