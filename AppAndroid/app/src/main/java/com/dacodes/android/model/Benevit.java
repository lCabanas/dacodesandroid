package com.dacodes.android.model;

import java.util.Date;
import java.util.List;

public class Benevit {
    public String description;
    public Date expiration_date;
    public Wallet wallet;
    public String vector_full_path;
    public boolean locked;
    public List<Territories> territories;
    public Ally ally;
}
