package com.dacodes.android.views.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dacodes.android.R;
import com.dacodes.android.model.Wallet;
import com.dacodes.android.views.adapters.holders.WalletHolder;

import java.util.List;

public class WalletAdapter extends RecyclerView.Adapter<WalletHolder> {

    List<Wallet> mList;
    Context mContext;

    public WalletAdapter(List<Wallet> mList, Context mContext) {
        this.mList = mList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public WalletHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View master = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_wallets, parent, false);
        WalletHolder mHolder = new WalletHolder(master);
        return mHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull WalletHolder holder, int position) {
        Wallet mItem = getItemAtPosition(position);
        holder.config(mItem, mContext);
    }

    @Override
    public int getItemCount() {
        return (mList == null ? 0 : mList.size());
    }

    public Wallet getItemAtPosition(int position) {
        if(position >= 0 && position < mList.size()){
            return mList.get(position);
        }
        return null;
    }
}
