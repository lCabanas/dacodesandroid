package com.dacodes.android.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.dacodes.android.R;
import com.dacodes.android.views.utils.ViewAnimation;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashActivity extends AppCompatActivity {

    @BindView(R.id.imageLogo)
    ImageView mImageViewLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ViewAnimation.showIn(mImageViewLogo);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent mIntent = new Intent(getApplicationContext(), LoginActivity.class);
                        startActivity(mIntent);
                        finish();
                    }
                }, 2000);

            }
        }, 1000);
    }
}
