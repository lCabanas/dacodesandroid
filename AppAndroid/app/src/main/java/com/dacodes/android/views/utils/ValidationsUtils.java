package com.dacodes.android.views.utils;

import android.util.Patterns;

public class ValidationsUtils {
    public static boolean validateEmail(String email) {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
}
