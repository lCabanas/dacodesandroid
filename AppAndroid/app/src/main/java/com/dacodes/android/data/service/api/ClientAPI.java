package com.dacodes.android.data.service.api;

import com.dacodes.android.data.service.request.LoginRequest;
import com.dacodes.android.data.service.response.BenevitsResponse;
import com.dacodes.android.data.service.response.LoginResponse;
import com.dacodes.android.model.Wallet;

import java.util.List;

import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface ClientAPI {
    @Headers({"Content-Type: application/json"})
    @POST("login")
    Call<LoginResponse> login(@Body LoginRequest json);

    @GET("member/wallets")
    Call<List<Wallet>> wallets();

    @GET("member/landing_benevits")
    Call<BenevitsResponse> benevits(@Header("Authorization") String token);

    @DELETE("logout")
    Call<ResponseBody> logout(@Header("Authorization") String token);
}
