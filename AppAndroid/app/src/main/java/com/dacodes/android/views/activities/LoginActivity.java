package com.dacodes.android.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.dacodes.android.R;
import com.dacodes.android.data.service.request.LoginRequest;
import com.dacodes.android.model.Member;
import com.dacodes.android.model.User;
import com.dacodes.android.presenter.LoginPresenter;
import com.dacodes.android.views.activities.base.BaseActivity;
import com.dacodes.android.views.utils.ValidationsUtils;
import com.dacodes.android.views.viewcontrollers.base.LoginViewController;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends BaseActivity implements LoginViewController {

    @BindView(R.id.username)
    EditText mEditTextUserName;

    @BindView(R.id.password)
    EditText mEditTextPassword;

    @BindView(R.id.login)
    Button mButtonLogin;

    LoginPresenter mPresenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        mPresenter = new LoginPresenter(this, LoginActivity.this);
        mEditTextUserName.addTextChangedListener(afterTextChangedListener);
        mEditTextPassword.addTextChangedListener(afterTextChangedListener);
        mEditTextPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_NEXT) {
                    login();
                    return true;
                }
                return false;
            }
        });

        mButtonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });
    }

    private boolean isValidText(){
        return (!mEditTextPassword.getText().toString().isEmpty() && !mEditTextUserName.getText().toString().isEmpty());
    }

    private void login(){
        if(isValidText()) {
            if (ValidationsUtils.validateEmail(mEditTextUserName.getText().toString())) {
                hideKeyboard();
                LoginRequest request = new LoginRequest();
                request.user = new User();
                request.user.email = mEditTextUserName.getText().toString();
                request.user.password = mEditTextPassword.getText().toString();
                mPresenter.login(request);
            } else {
                mEditTextUserName.setError(getString(R.string.error_invalid_email));
            }
        }
    }

    TextWatcher afterTextChangedListener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            // ignore
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            // ignore
        }

        @Override
        public void afterTextChanged(Editable s) {
            mButtonLogin.setEnabled(isValidText());
        }
    };

    @Override
    public void successLogin(Member member) {
        Intent mIntent = new Intent(getApplicationContext(), BenevitsActivity.class);
        startActivity(mIntent);
        finish();
    }

    @Override
    public void errorLogin(String error) {
        showInfoDialog(error, "Intente nuevamente" , "OK");
    }

    @Override
    public void showProgressDialog(String text) {
        showOnProgressDialog(text);
    }

    @Override
    public void hideProgressDialog() {
        dismissProgressDialog();
    }
}
