package com.dacodes.android.data.service;

import com.dacodes.android.data.service.api.ClientAPI;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ClientService {
    private Retrofit mRetrofit = null;
    private String URL = "https://staging.api.socioinfonavit.io/api/v1/";
    public static String TOKEN = "Authorization";
    public ClientAPI getAPI() {
        if (mRetrofit == null) {

            OkHttpClient client = new OkHttpClient
                    .Builder().connectTimeout(10, TimeUnit.HOURS)
                    .writeTimeout(10, TimeUnit.HOURS)
                    .readTimeout(10, TimeUnit.HOURS)
                    .build();

            Gson gson = new GsonBuilder()
                    .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                    .serializeNulls()
                    .create();

            mRetrofit = new Retrofit
                    .Builder()
                    .baseUrl(URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(client)
                    .build();
        }
        return mRetrofit.create(ClientAPI.class);
    }
}
