package com.dacodes.android.views.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.dacodes.android.R;
import com.dacodes.android.presenter.LogoutPresenter;
import com.dacodes.android.views.activities.base.BaseActivity;
import com.dacodes.android.views.utils.SharedPrefsUtils;
import com.dacodes.android.views.viewcontrollers.base.BenevitsMainViewController;
import com.google.android.material.navigation.NavigationView;

public class BenevitsActivity extends BaseActivity implements BenevitsMainViewController, NavigationView.OnNavigationItemSelectedListener {

    private AppBarConfiguration mAppBarConfiguration;
    private NavigationView navigationView;
    private LogoutPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_benevits);
        mPresenter = new LogoutPresenter(this, getApplicationContext());
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("");

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        if (navigationView != null) {
            navigationView.setNavigationItemSelectedListener(this);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();
        navigationView.getMenu().findItem(R.id.nav_home).setCheckable(true);
        navigationView.getMenu().findItem(R.id.nav_home).setChecked(true);

        switch (id) {
            case R.id.nav_logout:
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
                navigationView.setCheckedItem(0);

                showConfirmDialog("Confirmar", "¿Desea cerrar la sesión?", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mPresenter.logout();
                    }
                });
                break;
        }

        return true;
    }

    @Override
    public void showProgressDialog(String text) {
        showOnProgressDialog(text);
    }

    @Override
    public void hideProgressDialog() {
        dismissProgressDialog();
    }

    @Override
    public void successLogout() {
        SharedPrefsUtils.deleteAll(getApplicationContext());
        Intent mIntent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(mIntent);
        finish();
    }

    @Override
    public void errorLogout() {
        Toast.makeText(getApplicationContext(), "Ocurrió un error al tratar de cerrar la sesión", Toast.LENGTH_SHORT).show();
    }
}
