package com.dacodes.android.data.service.response;

import com.dacodes.android.data.service.response.base.BaseResponse;
import com.dacodes.android.model.Member;

public class LoginResponse extends BaseResponse {
    public int id;
    public String email;
    public Member member;
    public int sign_in_count;
}
