package com.dacodes.android.presenter;

import android.content.Context;

import com.dacodes.android.R;
import com.dacodes.android.data.service.ClientService;
import com.dacodes.android.data.service.request.LoginRequest;
import com.dacodes.android.data.service.response.LoginResponse;
import com.dacodes.android.views.utils.SharedPrefsUtils;
import com.dacodes.android.views.viewcontrollers.base.LoginViewController;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dacodes.android.data.service.ClientService.TOKEN;

public class LoginPresenter {
    private LoginViewController mViewController;
    private ClientService mClientService;
    private Context mContext;

    public LoginPresenter(LoginViewController mViewController, Context mContext) {
        this.mViewController = mViewController;
        this.mContext = mContext;
        if (this.mClientService == null) {
            this.mClientService = new ClientService();
        }
    }

    public void login(LoginRequest mRequest){
        mViewController.showProgressDialog("Espere un momento...");
        Gson mGson = new Gson();
        String json = mGson.toJson(mRequest);
        mClientService
                .getAPI()
                .login(mRequest)
                .enqueue(new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                        LoginResponse mResponse = response.body();
                        SharedPrefsUtils.setStringPreference(mContext, TOKEN, response.headers().get(TOKEN));
                        mViewController.hideProgressDialog();
                        if (mResponse != null) {
                            if (mResponse.isSuccess()) {
                                mViewController.successLogin(mResponse.member);
                            } else {
                                mViewController.errorLogin(mContext.getString(R.string.error_login));
                            }
                        } else {
                            mViewController.hideProgressDialog();
                            mViewController.errorLogin(mContext.getString(R.string.error_login));
                        }
                    }

                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {
                        mViewController.hideProgressDialog();
                        mViewController.errorLogin(mContext.getString(R.string.error_api));
                    }
                });
    }
}
